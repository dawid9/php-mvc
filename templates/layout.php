<!doctype html>
<html lang=pl>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- My styles -->
    <link rel="stylesheet" href="../src/styles/styles.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/css/mdb.min.css" rel="stylesheet">
    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>
    <title>Document</title>
</head>
<body>
    <!-- Material Desing Bootstrap container -->
    <div class="container-full-width">
        <div class="row">
            <div class="col-12">
                <!-- Header -->
                <nav class="navbar navbar-light primary-color lighten-4 mb-5">
                    <span class="navbar-brand text-white"> <i class="far fa-sticky-note"></i> Notatnik</span>
                </nav>
            </div>
        </div>

        <div class="container-full-width">
            <div class="row">

                <div class="col-3">
                    <ul class="list-group">
                        <li class="list-group-item border-0">
                            <div class="md-v-line"></div>
                            <i class="fas fa-list mr-4 pr-3"></i>
                            <a href="/">Lista notatek</a>
                        </li>
                        <li class="list-group-item border-0">
                            <div class="md-v-line"></div>
                            <i class="far fa-plus-square mr-4 pr-3"></i>
                            <a href="/?action=create">Nowa Notatka</a>
                        </li>

                    </ul>

                </div>

                <div class="col-8 col-md-offset-1" align="center" >
                    <?php
                        include_once("templates/pages/$page.php");
                    ?>
                </div>

            </div>
        </div>


        <!-- Footer -->
        <footer class="page-footer font-small primary-color fixed-bottom">
            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">© 2020 Copyright:
                <span>Dawid Czyż</span>
            </div>
            <!-- Copyright -->
        </footer>
        <!-- Footer -->
    </div>
    <!-- End container -->
</body>
</html>