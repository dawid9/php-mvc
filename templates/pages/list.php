
    <?php if(isset($viewParams['status']['notesCreated'])) : ?>
        <div class="alert alert-success" role="alert">
            Notatka została dodana pomyślnie
        </div>
    <?php endif; ?>

    <?php if(isset($viewParams['status']['notesEdited'])) : ?>
        <div class="alert alert-success" role="alert">
            Zmiany zapisane pomyślnie
        </div>
    <?php endif; ?>

    <?php if(isset($viewParams['status']['notesDeleted'])) : ?>
        <div class="alert alert-danger" role="alert">
            Notatka została usunięta
        </div>
    <?php endif; ?>

    <h3 class="text-center"> Lista notatek </h3>

    <table class="table table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th scope="col">Tytuł</th>
                <th scope="col">Opis</th>
                <th>Data utworzenia</th>
                <th>Opcje</th>
            </tr>
        </thead>
        <tbody>
         <?php foreach ($viewParams['notes'] ?? [] as $key) : ?>
            <tr>
                <th scope="row">#<?php echo (int) $key['id']; ?></th>
                <td><?php echo htmlentities($key['title']); ?></td>
                <td><?php echo htmlentities($key['description']); ?></td>
                <td><?php echo htmlentities($key['data']); ?></td>
                <td><a class="btn btn-primary btn-sm" href="/?action=show&id=<?php echo (int) $key['id']; ?>">Szczegóły</a></td>
            </tr>
         <?php endforeach; ?>
        </tbody>
    </table>




