
<h3 class="text-center"> Edytuj notatkę </h3>

<div class="col-5">

    <form action="/?action=edit&id= <?php echo $viewParams['id']; ?>" method="post">
        <label for="title">Tytuł</label>
        <input type="text" name="title" id="title" class="form-control" value="<?php echo $viewParams['title']; ?>">
        <label for="description">Opis</label>
        <textarea class="form-control" id="description" name="description" rows="3"><?php echo $viewParams['description']; ?></textarea>
        <input type="submit" class="btn btn-primary my-2" value="Zapisz zmiany">
        <a class="btn btn-primary" href="/?">Anuluj</a>
    </form>

</div>
