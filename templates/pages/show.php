
<div class="card col-5 text-left border-0">

  <div class="card-body primary-color white-text rounded-bottom">

    <h4 class="card-title">
        #<?php echo $viewParams['id']; ?>
        <?php echo $viewParams['title']; ?>
    </h4>
      <i><?php echo $viewParams['data']; ?></i>
    <hr class="hr-light">

    <p class="card-text white-text mb-4">
        <?php echo $viewParams['description']; ?>
    </p>

    <a href="/?action=edit&id=<?php echo $viewParams['id']; ?>" class="white-text d-flex justify-content-end">
      <h5>Edytuj <i class="fas fa-angle-double-right"></i></h5>
    </a>

      <a href="/?action=delete&id=<?php echo $viewParams['id']; ?>" class="white-text d-flex justify-content-end">
          <h5>Usuń <i class="fas fa-angle-double-right"></i></h5>
      </a>

  </div>

</div> <!-- end card -->
