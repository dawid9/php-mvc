<?php
declare(strict_types=1);

namespace App\Src\Utils\HelpFunctions;

function redirect(string $url){
    if(!empty($url)){
        header("Location: $url");
    }else{
        return;
    }
}