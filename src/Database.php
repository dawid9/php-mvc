<?php
declare(strict_types=1);

namespace App;

use App\AbstractController;
use PDO;
use Exception;
use PDOException;

require_once "AbstractDatabase.php";

class dbConnection extends AbstractDatabase
{
    public function createNote(array $data): bool
    {

        $title          = (string) !empty($data['title']) ? $data['title'] : "Bez tytułu";
        $description    = (string) $data['description'];

        try{
            $query = "INSERT INTO ".self::TABLE_NAME." (title, description) VALUES('$title', '$description')" ;
            $this->db->exec($query);
            return true;

        }catch (Exception $e){
            echo "Connection failed: " . $e->getMessage();
            return false;
        }
    }
    public function getNotes(): array
    {
        $notes = array();
        $query = "SELECT * FROM ".self::TABLE_NAME;

        try{
            $result = $this->db->query($query);
            $notes = $result->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }

        return $notes;
    }
    public function getNote(int $id): array
    {
        try{
            $query = "SELECT * FROM ".self::TABLE_NAME." WHERE id='$id'";
            $result = $this->db->query($query);
            $note = $result->fetch(PDO::FETCH_ASSOC);

            return $note;

        }catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
            return [];
        }
    }
    public function editNote(array $data): bool
    {
        $id          = (int) $data['id'] ?? null;
        $title       = (string) $data['title'];
        $description = (string) $data['description'];

        try{
            $query = "
                UPDATE ".self::TABLE_NAME." 
                SET title='$title', description='$description'
                WHERE id=$id 
                LIMIT 1
            ";

            if($this->db->exec($query)){
                return true;
            }

        }catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
            return false;
        }
    }

    public function deleteNote(int $id): bool
    {
        $id = (int) $id ?? null;

        try{

            $query = "DELETE FROM ".self::TABLE_NAME." WHERE id=$id LIMIT 1";
            if($this->db->exec($query)){
                return true;
            }

        }catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
            return false;
        }
    }
}



