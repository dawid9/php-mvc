<?php
declare(strict_types=1);

namespace App;

use App\AbstractController;
use App\Request;

require_once "AbstractController.php";

class Controller extends AbstractController
{
    public function run(View $view): void
    {
        $view = $view;
        switch ($this->action()){
            case "create" :
                $this->create($view);
                break;

            case "show" :
                $this->show($view);
                break;

            case "edit" :
                $this->edit($view);
                break;

            case "delete" :
                $this->delete($view);
                break;

            default:
                $this->lists($view);
                break;
        }
    }
    public function create(View $view): void
    {
        $viewParams = array();
        $page = "create";
        $dataPost = $this->request->requestCheck("post");
        if(!empty($dataPost)){
            $viewParams = array(
                "title" => $dataPost['title'],
                "description" => $dataPost['description']
            );

            if($this->dbConnection->createNote($viewParams)){
                header("Location: /?notesCreated=true");
            }

        }

        $view->render($page, $viewParams);
    }
    public function show(View $view): void
    {
        $page = "show";
        $data = $this->request->requestCheck("get");
        $id = (int) $data['id'] ?? null;
        $viewParams = $this->dbConnection->getNote($id);
        $view->render($page, $viewParams);
    }
    public function lists(View $view): void
    {
        $page = "list";
        // Read notes from DB
        $viewParams = array(
            "notes" => $this->dbConnection->getNotes(),
            "status" => $this->request->requestCheck("get")
        );
        $view->render($page, $viewParams);
    }
    public function edit(View $view): void
    {
        $page = "edit";
        $data = $this->request->requestCheck("get");
        $id = (int) $data['id'] ?? null;
        $viewParams = $this->dbConnection->getNote($id);

        $dataPost = $this->request->requestCheck("post");
        if(!empty($dataPost)){
            $viewParams = array(
                "id" => $id,
                "title" => $dataPost['title'],
                "description" => $dataPost['description']
            );

            if($this->dbConnection->editNote($viewParams)){
                header("Location: /?notesEdited=true");
            }
        }

        $view->render($page, $viewParams);
    }
    public function delete(View $view): void
    {
        $data = $this->request->requestCheck("get");
        $id = (int) $data['id'] ?? null;

        if($this->dbConnection->deleteNote($id)){
            header("Location: /?notesDeleted=true");
        }
    }

    private function action(): string
    {
        $data = $this->request->requestCheck("get");
        return $data['action'] ?? self::DEFAULT_ACTION;
    }

}



