<?php
declare(strict_types=1);

namespace App;

use App\View;
use App\dbConnection;
use App\Request;

require_once "src/View.php";
require_once "src/Database.php";
require_once "src/Request.php";

abstract class AbstractController{

    protected const DEFAULT_ACTION = "list";
    protected $request;
    protected $dbConnection;

    public function __construct(Request $request, dbConnection $conn)
    {
        $this->request      = $request;
        $this->dbConnection = $conn;
    }
}