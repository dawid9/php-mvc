<?php
declare(strict_types=1);

namespace App;

use Cassandra\Date;
use PDO;
use PDOException;
use Exception;
use DateTime;

require_once "config/config.php";

abstract class AbstractDatabase
{
    protected const TABLE_NAME = "demo";
    protected string $host;
    protected string $user;
    protected string $password;

    protected PDO $db;

    public function __construct(array $db)
    {
        $this->host     = $db['host'];
        $this->user     = $db['user'];
        $this->password = $db['password'];

        try{
            $this->db = new PDO($this->host, $this->user, $this->password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }catch (PDOException $e){
            echo "Connection failed: " . $e->getMessage();
        }
    }
}