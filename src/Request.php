<?php
declare(strict_types=1);

namespace App;

class Request{

    public array $get   = array();
    public array $post  = array();

    public function __construct(array $get, array $post)
    {
        $this->get  = $get;
        $this->post = $post;
    }


    public function requestCheck(string $name): ?array
    {
        return $this->$name ?? null;
    }

}