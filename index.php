<?php
declare(strict_types=1);

namespace App;

use App\Controller;

require_once "src/Controller/Controller.php";

$view = new View();
$dbconn = new dbConnection($dbAccess);
$request = new Request($_GET, $_POST);

(new Controller($request, $dbconn))->run($view);







